const $galWrapper = $('.gal-wrapper');
const timeAnimation = 500;
const scrollWrapper = $('.gal').width();
$('#next-btn').click(() => {
	if(!$galWrapper.is(':animated')) {
		$galWrapper
		.animate({'margin-left': -scrollWrapper}, timeAnimation, () => {
			$('.slider_img:first')
			.appendTo($galWrapper)
			.parent()
			.css({'margin-left':0});
		});		
	}		
});
$('#prev-btn').click(() => {
	if (!$galWrapper.is(':animated')) {
		$galWrapper
		.css({'margin-left': -scrollWrapper})
		.find('.slider_img:last')
		.prependTo($galWrapper)
		.parent()
		.animate({'margin-left': 0}, timeAnimation);
	}
}); 